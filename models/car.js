"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class car extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.car.belongsTo(models.category, {
        foreignKey: "category_id",
        as: "cat_id",
      });
      models.car.belongsTo(models.detail, {
        foreignKey: "car_id",
      });
    }
  }
  car.init(
    {
      name: DataTypes.STRING,
      category_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "car",
    }
  );
  return car;
};
