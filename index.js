const express = require("express"); // import express
const app = express(); // create express app
const { Cars } = require("./models");

const port = process.env.PORT || 8000; // set port
app.use(express.json()); // use json
app.use(express.static("public")); // use static files

app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});

// Cars
app.get("/api/v1/cars", async (req, res) => {
  try {
    const cars = await Cars.findAll();
    res.status(200).json(cars);
  } catch (err) {
    res.status(500).json({
      message: err.message,
    });
  }
});

// Post Cars
app.post("/api/v1/add", (req, res) => {
  Cars.create(req.body)
    .then((cars) => {
      res.status(201).json(cars);
    })
    .catch((err) => {
      res.status(500).json({
        err: "Tidak dapat menambahkan mobil",
      });
    });
});
